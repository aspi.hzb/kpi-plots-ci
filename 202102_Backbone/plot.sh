#!/bin/bash

# make sure that the data we need is filtered before plotting
pushd ../data/aai_users_per_centre/
./filter.sh
popd

pushd ../data/aai_users/
./filter.sh
popd

# the following filtering step is legacy
pushd ../stats/aai/

# summing up columns 2 and 3, combined
in=aai_userstats.csv
out=aai_users.csv
if [ $in -nt $out ]; then
  cat $in | awk 'BEGIN{FS=","}{print $1","a; a+=$2+$3}' > $out
fi


# summing up columns 2,3,4 and 5
# leaving first (date) and columns after 5 (service name[s]) unchanged.
in=HIFIS_AAI_SP.csv
out=aai_services.csv
if [ $in -nt $out ]; then
  cat $in | awk 'BEGIN{FS=","}{
    printf ("%s,",$1);
    
    for (a=2; a<=5; a++)
    {
        sum[a]+=$a;
        printf ("%g,",sum[a]);
    }
    
    for (a=6; a<=NF; a++)
        printf ("%s,",$a);
    
    printf("\n");
    
  }' > $out
fi

popd

# NOW do the plotting
gnuplot ./plot.plt

# convert to png... word word, pptx, etc
for i in plot*svg; do
  convert "$i" -dither none PNG8:"${i%.svg}.png"
done

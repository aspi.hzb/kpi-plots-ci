#!/bin/gnuplot

set datafile separator ","
set output 'plot.svg'


# Test for number of columns
# The stats command does not work in timefmt format, unfortunately.
# Since we only want to get the number of columns, we can ignore the "bad data on line 1 of file ../data/aai_users_per_centre/data_cumulated.csv" message
fncumulated='../data/aai_users_per_centre/data_cumulated.csv'
stats fncumulated nooutput
print "Detected ".STATS_columns." columns."

set yrange [0:*]

set xdata time
set timefmt "%Y-%m-%d %H:%M"
set format x "%Y-%m-%d"
set xrange ["2019-07-01":*]
set grid 
set xtics rotate by 45


set terminal svg size 1100,800
if (!exists("MP_LEFT"))   MP_LEFT = .05
if (!exists("MP_RIGHT"))  MP_RIGHT = .99
if (!exists("MP_BOTTOM")) MP_BOTTOM = .09
if (!exists("MP_TOP"))    MP_TOP = .99
if (!exists("MP_xGAP"))   MP_xGAP = 0.07
if (!exists("MP_yGAP"))   MP_yGAP = 0.02

set multiplot layout 2,2 columnsfirst margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP

set linetype 2 dashtype 2

set timefmt "%Y-%m-%d %H:%M"
set key top left inside font "Helvetica,9" reverse Left
set xtics textcolor rgbcolor "white"
set ylabel "{/Arial:Bold B2a V2}: Institutions in AAI" offset 1,0
set yrange [0:*]
plot '../data/aai_centers/data_plot.csv' u 1:($2>0?$2:1/0) w lp pt 2 ps 0.5 lc black title 'Helmholtz IdPs' ,\
     ''                                  u 1:($2<=11?$2:1/0) w lp pt 6 ps 0.5 lc black lt 2 notitle ,\
     '../data/aai_domains_stats/data_plot.csv' u 1:4 w lp pt 7 ps 0.5 lc rgb "#005aa0" title 'Distinct domains (20+ users)'

# excluding functional accounts, we have some 150-200 real user accounts less than in the summarized report until
# beginning of 2023. To avoid (non-telling) jumps and glitches in the plots: add a -2% correction for all data before 2023
# this is just for smooth plotting. raw (csv) data is untouched.
old_stats_factor=13282./13571.   # with data from 2023-01-01

set yrange [0:*]
set timefmt "%Y-%m-%d %H:%M"
set xtics textcolor rgbcolor "black" offset -4,-3
set ylabel '{/Arial:Bold B2c}: Registered end users (thousand)'
set key top left inside font "Helvetica,9" reverse Left
set label "No centre-\nspecific\ndata available" at (2021.55-1970)*365.25*24*3600,2000 center font "Helvetica,9"
plot '../data/aai_users/data_plot_2.csv' u 1:($2>=5790?$2/1000.:1/0) w filledcurves x1 lc rgb "#aaaacc" title 'Non Helmholtz IdP' ,\
     '../data/aai_users/data_plot.csv'   u 1:($2>=5790 && $2<12500?$2*old_stats_factor/1000.:1/0) w filledcurves x1 lc rgb "#aaaacc" notitle ,\
     '../data/aai_users/data_plot_2.csv' u 1:($2/1000.) w lines lt 1 lw 2 lc rgb "#000000" notitle ,\
     '../data/aai_users/data_plot.csv'   u 1:($2<12500?$2*old_stats_factor/1000.:1/0) w lines lt 1 lw 2 lc rgb "#000000" notitle ,\
     for [i=STATS_columns:2:-1] fncumulated u 1:(column(i)/1000.) w filledcurves x1 title columnheader
unset label

set timefmt "%d.%m.%y"
set xtics textcolor rgbcolor "white"
set ylabel "{/Arial:Bold B2b}: AAI-connected services"
plot '../stats/aai/aai_services.csv' u 1:2 w lp pt 7 ps 0.5 lc black notitle

set key top left
set timefmt "%d.%m.%Y"
set xtics textcolor rgbcolor "black" offset -4,-3
set ylabel '{/Arial:Bold B2d}: Registered top level VOs'
plot '../stats/aai/aai_vo.csv' u 1:2 w lp pt 7 ps 0.5 lt 1 lc black notitle
set timefmt "%Y-%m-%d %H:%M"

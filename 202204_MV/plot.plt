#!/bin/gnuplot

set datafile separator ","
set output './plot.svg'

set yrange [0:*]

set xdata time
set timefmt "%Y-%m-%d %H:%M"
set format x "%Y-%m-%d"
set xrange ["2019-07-01":*]
set xtics rotate by 45 offset -4,-3
set ytics nomirror
set key top left
set border 3


set terminal svg size 400,600
if (!exists("MP_LEFT"))   MP_LEFT = 0.18
if (!exists("MP_RIGHT"))  MP_RIGHT = 0.85
if (!exists("MP_BOTTOM")) MP_BOTTOM = .12
if (!exists("MP_TOP"))    MP_TOP = .99
if (!exists("MP_xGAP"))   MP_xGAP = 0.07
if (!exists("MP_yGAP"))   MP_yGAP = 0.03


set multiplot layout 2,1 columnsfirst margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP


set timefmt "%d.%m.%Y"
set ylabel "Helmholtz AAI users" offset 1
set xtics textcolor rgbcolor "white" nomirror
plot '../stats/aai/aai_users.csv'       u 1:2 w l lc black lw 2 notitle

set border 11
set datafile separator ","     
set ylabel "Cumulative course participant-hours"
set y2label "Per-month participant-hours" offset -1.5,0
set y2tics
set xtics textcolor rgbcolor "black" offset -4,-3 nomirror
set timefmt "%Y-%m"
set style fill pattern 6
plot '../stats/workshops/data.csv' u 1:2 w boxes axis x1y2 lc rgb "#005aa0" lw 0.8 title 'per month' ,\
     ''                                                     u 1:3 w l lc black lw 2 title 'cumulative'
     
unset multiplot

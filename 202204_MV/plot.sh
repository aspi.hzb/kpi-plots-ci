#!/bin/bash

gnuplot plot.plt

# convert to png... word word, pptx, etc
for i in plot*svg; do
  convert "$i" -dither none PNG8:"${i%.svg}.png"
done

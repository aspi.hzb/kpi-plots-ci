# Example graphs
The plots have been used in previous reports and are updated with most recent data here. See also [static plots here](https://codebase.helmholtz.cloud/hifis/overall/communication/service-usage-plots/-/tree/backup-of-plot-data).

##### Backbone cluster KPI plot 2021
* ![backbone_plot_1](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/202102_Backbone/plot.png?job=plot_general)

##### Overall Report KPI plot 2021
* ![overall_plot_1](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/202103_Report_Overall/Report2020_plot1.png?job=plot_general)

##### MV 2021-04, 2021-09, and 2022-04
* ![mv_plot_1](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/202204_MV/plot.png?job=plot_general)

##### Overall Report KPI plot 2022
* ![overall_plot_1](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/202203_Report_Overall/Report2021_plot1.png?job=plot_general)

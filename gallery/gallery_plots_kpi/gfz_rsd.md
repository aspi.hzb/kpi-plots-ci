# RSD (GFZ)

##### gfz_rsd-plot_10.svg
![gfz_rsd-plot_10.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_rsd-plot_10.svg?job=plot_general)

##### gfz_rsd-plot_2.svg
![gfz_rsd-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_rsd-plot_2.svg?job=plot_general)

##### gfz_rsd-plot_3.svg
![gfz_rsd-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_rsd-plot_3.svg?job=plot_general)

##### gfz_rsd-plot_4.svg
![gfz_rsd-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_rsd-plot_4.svg?job=plot_general)

##### gfz_rsd-plot_5.svg
![gfz_rsd-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_rsd-plot_5.svg?job=plot_general)

##### gfz_rsd-plot_6.svg
![gfz_rsd-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_rsd-plot_6.svg?job=plot_general)

##### gfz_rsd-plot_7.svg
![gfz_rsd-plot_7.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_rsd-plot_7.svg?job=plot_general)

##### gfz_rsd-plot_8.svg
![gfz_rsd-plot_8.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_rsd-plot_8.svg?job=plot_general)

##### gfz_rsd-plot_9.svg
![gfz_rsd-plot_9.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_rsd-plot_9.svg?job=plot_general)


# nubes (HZDR)

##### hzb_nubes-plot_10.svg
![hzb_nubes-plot_10.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzb_nubes-plot_10.svg?job=plot_general)

##### hzb_nubes-plot_2.svg
![hzb_nubes-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzb_nubes-plot_2.svg?job=plot_general)

##### hzb_nubes-plot_3.svg
![hzb_nubes-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzb_nubes-plot_3.svg?job=plot_general)

##### hzb_nubes-plot_4.svg
![hzb_nubes-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzb_nubes-plot_4.svg?job=plot_general)

##### hzb_nubes-plot_5.svg
![hzb_nubes-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzb_nubes-plot_5.svg?job=plot_general)

##### hzb_nubes-plot_6.svg
![hzb_nubes-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzb_nubes-plot_6.svg?job=plot_general)

##### hzb_nubes-plot_7.svg
![hzb_nubes-plot_7.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzb_nubes-plot_7.svg?job=plot_general)

##### hzb_nubes-plot_8.svg
![hzb_nubes-plot_8.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzb_nubes-plot_8.svg?job=plot_general)

##### hzb_nubes-plot_9.svg
![hzb_nubes-plot_9.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzb_nubes-plot_9.svg?job=plot_general)


# HIFIS Events (DESY)

##### desy_events-plot_2.svg
![desy_events-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_events-plot_2.svg?job=plot_general)

##### desy_events-plot_3.svg
![desy_events-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_events-plot_3.svg?job=plot_general)

##### desy_events-plot_4.svg
![desy_events-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_events-plot_4.svg?job=plot_general)

##### desy_events-plot_5.svg
![desy_events-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_events-plot_5.svg?job=plot_general)

##### desy_events-plot_6.svg
![desy_events-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_events-plot_6.svg?job=plot_general)


# Sensor Mgmt Sys (GFZ)

##### gfz_sensor_mgmt-plot_10.svg
![gfz_sensor_mgmt-plot_10.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_sensor_mgmt-plot_10.svg?job=plot_general)

##### gfz_sensor_mgmt-plot_11.svg
![gfz_sensor_mgmt-plot_11.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_sensor_mgmt-plot_11.svg?job=plot_general)

##### gfz_sensor_mgmt-plot_12.svg
![gfz_sensor_mgmt-plot_12.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_sensor_mgmt-plot_12.svg?job=plot_general)

##### gfz_sensor_mgmt-plot_13.svg
![gfz_sensor_mgmt-plot_13.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_sensor_mgmt-plot_13.svg?job=plot_general)

##### gfz_sensor_mgmt-plot_14.svg
![gfz_sensor_mgmt-plot_14.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_sensor_mgmt-plot_14.svg?job=plot_general)

##### gfz_sensor_mgmt-plot_2.svg
![gfz_sensor_mgmt-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_sensor_mgmt-plot_2.svg?job=plot_general)

##### gfz_sensor_mgmt-plot_3.svg
![gfz_sensor_mgmt-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_sensor_mgmt-plot_3.svg?job=plot_general)

##### gfz_sensor_mgmt-plot_4.svg
![gfz_sensor_mgmt-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_sensor_mgmt-plot_4.svg?job=plot_general)

##### gfz_sensor_mgmt-plot_5.svg
![gfz_sensor_mgmt-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_sensor_mgmt-plot_5.svg?job=plot_general)

##### gfz_sensor_mgmt-plot_6.svg
![gfz_sensor_mgmt-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_sensor_mgmt-plot_6.svg?job=plot_general)

##### gfz_sensor_mgmt-plot_7.svg
![gfz_sensor_mgmt-plot_7.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_sensor_mgmt-plot_7.svg?job=plot_general)

##### gfz_sensor_mgmt-plot_8.svg
![gfz_sensor_mgmt-plot_8.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_sensor_mgmt-plot_8.svg?job=plot_general)

##### gfz_sensor_mgmt-plot_9.svg
![gfz_sensor_mgmt-plot_9.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/gfz_sensor_mgmt-plot_9.svg?job=plot_general)


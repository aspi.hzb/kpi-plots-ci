# Juypter (FZJ)

##### fzj_jupyter-plot_2.svg
![fzj_jupyter-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/fzj_jupyter-plot_2.svg?job=plot_general)

##### fzj_jupyter-plot_3.svg
![fzj_jupyter-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/fzj_jupyter-plot_3.svg?job=plot_general)

##### fzj_jupyter-plot_4.svg
![fzj_jupyter-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/fzj_jupyter-plot_4.svg?job=plot_general)

##### fzj_jupyter-plot_5.svg
![fzj_jupyter-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/fzj_jupyter-plot_5.svg?job=plot_general)


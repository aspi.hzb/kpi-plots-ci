# software_workshops

##### software_workshops-plot_2.svg
![software_workshops-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/software_workshops-plot_2.svg?job=plot_general)

##### software_workshops-plot_3.svg
![software_workshops-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/software_workshops-plot_3.svg?job=plot_general)

##### software_workshops-plot_4.svg
![software_workshops-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/software_workshops-plot_4.svg?job=plot_general)


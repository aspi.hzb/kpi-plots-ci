# Codebase (HZDR)

##### hzdr_codebase_active_users-plot_2.svg
![hzdr_codebase_active_users-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_active_users-plot_2.svg?job=plot_general)

##### hzdr_codebase_active_users-plot_3.svg
![hzdr_codebase_active_users-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_active_users-plot_3.svg?job=plot_general)

##### hzdr_codebase_active_users-plot_4.svg
![hzdr_codebase_active_users-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_active_users-plot_4.svg?job=plot_general)

##### hzdr_codebase_active_users-plot_5.svg
![hzdr_codebase_active_users-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_active_users-plot_5.svg?job=plot_general)

##### hzdr_codebase_active_users-plot_6.svg
![hzdr_codebase_active_users-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_active_users-plot_6.svg?job=plot_general)

##### hzdr_codebase_active_users-plot_7.svg
![hzdr_codebase_active_users-plot_7.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_active_users-plot_7.svg?job=plot_general)

##### hzdr_codebase_project_statistics-plot_10.svg
![hzdr_codebase_project_statistics-plot_10.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_project_statistics-plot_10.svg?job=plot_general)

##### hzdr_codebase_project_statistics-plot_11.svg
![hzdr_codebase_project_statistics-plot_11.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_project_statistics-plot_11.svg?job=plot_general)

##### hzdr_codebase_project_statistics-plot_12.svg
![hzdr_codebase_project_statistics-plot_12.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_project_statistics-plot_12.svg?job=plot_general)

##### hzdr_codebase_project_statistics-plot_13.svg
![hzdr_codebase_project_statistics-plot_13.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_project_statistics-plot_13.svg?job=plot_general)

##### hzdr_codebase_project_statistics-plot_2.svg
![hzdr_codebase_project_statistics-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_project_statistics-plot_2.svg?job=plot_general)

##### hzdr_codebase_project_statistics-plot_3.svg
![hzdr_codebase_project_statistics-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_project_statistics-plot_3.svg?job=plot_general)

##### hzdr_codebase_project_statistics-plot_4.svg
![hzdr_codebase_project_statistics-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_project_statistics-plot_4.svg?job=plot_general)

##### hzdr_codebase_project_statistics-plot_5.svg
![hzdr_codebase_project_statistics-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_project_statistics-plot_5.svg?job=plot_general)

##### hzdr_codebase_project_statistics-plot_6.svg
![hzdr_codebase_project_statistics-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_project_statistics-plot_6.svg?job=plot_general)

##### hzdr_codebase_project_statistics-plot_7.svg
![hzdr_codebase_project_statistics-plot_7.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_project_statistics-plot_7.svg?job=plot_general)

##### hzdr_codebase_project_statistics-plot_8.svg
![hzdr_codebase_project_statistics-plot_8.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_project_statistics-plot_8.svg?job=plot_general)

##### hzdr_codebase_project_statistics-plot_9.svg
![hzdr_codebase_project_statistics-plot_9.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_project_statistics-plot_9.svg?job=plot_general)


# Rancher k8s (DESY)

##### desy_rancher-plot_2.svg
![desy_rancher-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_rancher-plot_2.svg?job=plot_general)

##### desy_rancher-plot_3.svg
![desy_rancher-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_rancher-plot_3.svg?job=plot_general)

##### desy_rancher-plot_4.svg
![desy_rancher-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_rancher-plot_4.svg?job=plot_general)

##### desy_rancher-plot_5.svg
![desy_rancher-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_rancher-plot_5.svg?job=plot_general)

##### desy_rancher-plot_6.svg
![desy_rancher-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_rancher-plot_6.svg?job=plot_general)

##### desy_rancher-plot_7.svg
![desy_rancher-plot_7.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_rancher-plot_7.svg?job=plot_general)

##### desy_rancher-plot_8.svg
![desy_rancher-plot_8.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_rancher-plot_8.svg?job=plot_general)

##### desy_rancher-plot_9.svg
![desy_rancher-plot_9.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_rancher-plot_9.svg?job=plot_general)


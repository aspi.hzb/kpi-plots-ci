# Helmholtz AAI

##### aai_active_users-plot_2.svg
![aai_active_users-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/aai_active_users-plot_2.svg?job=plot_general)

##### aai_centers-plot_2.svg
![aai_centers-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/aai_centers-plot_2.svg?job=plot_general)

##### aai_domains-plot_cumulated_with_overall.svg
![aai_domains-plot_cumulated_with_overall.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/aai_domains-plot_cumulated_with_overall.svg?job=plot_general)

##### aai_domains_stats-plot_2.svg
![aai_domains_stats-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/aai_domains_stats-plot_2.svg?job=plot_general)

##### aai_domains_stats-plot_3.svg
![aai_domains_stats-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/aai_domains_stats-plot_3.svg?job=plot_general)

##### aai_domains_stats-plot_4.svg
![aai_domains_stats-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/aai_domains_stats-plot_4.svg?job=plot_general)

##### aai_services-plot_2.svg
![aai_services-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/aai_services-plot_2.svg?job=plot_general)

##### aai_users-plot_2.svg
![aai_users-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/aai_users-plot_2.svg?job=plot_general)

##### aai_users_per_centre-plot_cumulated.svg
![aai_users_per_centre-plot_cumulated.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/aai_users_per_centre-plot_cumulated.svg?job=plot_general)

##### aai_users_per_centre-plot_cumulated_with_overall.svg
![aai_users_per_centre-plot_cumulated_with_overall.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/aai_users_per_centre-plot_cumulated_with_overall.svg?job=plot_general)

##### aai_vo-plot_2.svg
![aai_vo-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/aai_vo-plot_2.svg?job=plot_general)


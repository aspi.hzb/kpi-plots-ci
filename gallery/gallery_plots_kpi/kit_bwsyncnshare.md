# bwSyncnShare (KIT)

##### kit_bwsyncnshare-plot_2.svg
![kit_bwsyncnshare-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/kit_bwsyncnshare-plot_2.svg?job=plot_general)

##### kit_bwsyncnshare-plot_3.svg
![kit_bwsyncnshare-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/kit_bwsyncnshare-plot_3.svg?job=plot_general)

##### kit_bwsyncnshare-plot_4.svg
![kit_bwsyncnshare-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/kit_bwsyncnshare-plot_4.svg?job=plot_general)

##### kit_bwsyncnshare-plot_5.svg
![kit_bwsyncnshare-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/kit_bwsyncnshare-plot_5.svg?job=plot_general)


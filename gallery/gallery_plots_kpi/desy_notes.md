# Notes (DESY)

##### desy_notes_daily-plot_2.svg
![desy_notes_daily-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_notes_daily-plot_2.svg?job=plot_general)

##### desy_notes_daily-plot_3.svg
![desy_notes_daily-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_notes_daily-plot_3.svg?job=plot_general)

##### desy_notes_daily-plot_4.svg
![desy_notes_daily-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_notes_daily-plot_4.svg?job=plot_general)

##### desy_notes_daily-plot_5.svg
![desy_notes_daily-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_notes_daily-plot_5.svg?job=plot_general)

##### desy_notes_daily-plot_6.svg
![desy_notes_daily-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_notes_daily-plot_6.svg?job=plot_general)

##### desy_notes_weekly-plot_2.svg
![desy_notes_weekly-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_notes_weekly-plot_2.svg?job=plot_general)

##### desy_notes_weekly-plot_3.svg
![desy_notes_weekly-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_notes_weekly-plot_3.svg?job=plot_general)

##### desy_notes_weekly-plot_4.svg
![desy_notes_weekly-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_notes_weekly-plot_4.svg?job=plot_general)

##### desy_notes_weekly-plot_5.svg
![desy_notes_weekly-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_notes_weekly-plot_5.svg?job=plot_general)

##### desy_notes_weekly-plot_6.svg
![desy_notes_weekly-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/desy_notes_weekly-plot_6.svg?job=plot_general)


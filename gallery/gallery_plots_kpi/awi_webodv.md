# webODV (AWI)

##### awi_webodv-plot_2.svg
![awi_webodv-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/awi_webodv-plot_2.svg?job=plot_general)

##### awi_webodv-plot_3.svg
![awi_webodv-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/awi_webodv-plot_3.svg?job=plot_general)

##### awi_webodv-plot_4.svg
![awi_webodv-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/awi_webodv-plot_4.svg?job=plot_general)

##### awi_webodv-plot_5.svg
![awi_webodv-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/awi_webodv-plot_5.svg?job=plot_general)

##### awi_webodv-plot_6.svg
![awi_webodv-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/awi_webodv-plot_6.svg?job=plot_general)

##### awi_webodv-plot_7.svg
![awi_webodv-plot_7.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/awi_webodv-plot_7.svg?job=plot_general)

##### awi_webodv-plot_8.svg
![awi_webodv-plot_8.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/awi_webodv-plot_8.svg?job=plot_general)

##### awi_webodv-plot_9.svg
![awi_webodv-plot_9.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/awi_webodv-plot_9.svg?job=plot_general)


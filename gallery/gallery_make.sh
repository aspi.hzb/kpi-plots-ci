#!/bin/bash

# produce static gallery file for quick-and-dirty view of all available svg files.
# so far, this needs to be done locally and result needs to be committed manually.


mkdir -pv gallery_plots gallery_plots_kpi

galleryfile_overview=gallery.md

> $galleryfile_overview

echo -e "# Overview of plots\n" > $galleryfile_overview
servicename="."
for fn in ../plots/svg/*.svg; do
  plotname=$(echo $fn | cut -d "/" -f 4)
  servicename_new=$(echo $plotname | cut -d "-" -f 1)
  servicegallery=gallery_plots/${servicename_new}.md
  
  # Replace auto-extracted servicename with displayname if there is any in the metadata
  dnpath=../data/${servicename_new}/service_metadata
  if [ -e "$dnpath" ]; then
  if [ -e "$dnpath"//displayname ]; then
     servicename_new=$(cat "$dnpath"/displayname)
     servicegallery=gallery_plots/$(cd "$dnpath"; pwd -P | rev | cut -d "/" -f 1 | rev).md
  fi
  fi
  
  # new file for each service
  if [ "$servicename_new" != "$servicename" ]; then
     servicename="$servicename_new"
     echo -e "# "$servicename"\n" > $servicegallery

     # and add a link to this file in the overview:
     echo "- ["$servicename"]("$servicegallery")" >> $galleryfile_overview
  fi

  {
    echo "##### $plotname"
    echo "!["$plotname"](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/"$plotname"?job=plot_general)"
    echo
  } >> $servicegallery
done


# same for the KPI plots

echo -e "\n# Overview of KPI plots\n" >> $galleryfile_overview
servicename="."
for fn in ../plots_kpi/svg/*.svg; do
  plotname=$(echo $fn | cut -d "/" -f 4)
  servicename_new=$(echo $plotname | cut -d "-" -f 1)
  servicegallery=gallery_plots_kpi/${servicename_new}.md
  
  # Replace auto-extracted servicename with displayname if there is any in the metadata
  dnpath=../data/${servicename_new}/service_metadata
  if [ -e "$dnpath" ]; then
  if [ -e "$dnpath"//displayname ]; then
     servicename_new=$(cat "$dnpath"/displayname)
     servicegallery=gallery_plots_kpi/$(cd "$dnpath"; pwd -P | rev | cut -d "/" -f 1 | rev).md
  fi
  fi
  
  # new file for each service
  if [ "$servicename_new" != "$servicename" ]; then
     servicename="$servicename_new"
     echo -e "# "$servicename"\n" > $servicegallery

     # and add a link to this file in the overview:
     echo "- ["$servicename"]("$servicegallery")" >> $galleryfile_overview
  fi

  {
    echo "##### $plotname"
    echo "!["$plotname"](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/"$plotname"?job=plot_general)"
    echo
  } >> $servicegallery
done

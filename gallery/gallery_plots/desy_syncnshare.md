# SyncnShare (DESY)

##### desy_syncnshare_consumption-plot_2.svg
![desy_syncnshare_consumption-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/desy_syncnshare_consumption-plot_2.svg?job=plot_general)

##### desy_syncnshare_consumption-plot_3.svg
![desy_syncnshare_consumption-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/desy_syncnshare_consumption-plot_3.svg?job=plot_general)

##### desy_syncnshare_consumption-plot_4.svg
![desy_syncnshare_consumption-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/desy_syncnshare_consumption-plot_4.svg?job=plot_general)

##### desy_syncnshare_consumption-plot_5.svg
![desy_syncnshare_consumption-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/desy_syncnshare_consumption-plot_5.svg?job=plot_general)

##### desy_syncnshare_consumption-plot_6.svg
![desy_syncnshare_consumption-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/desy_syncnshare_consumption-plot_6.svg?job=plot_general)

##### desy_syncnshare_consumption-plot_7.svg
![desy_syncnshare_consumption-plot_7.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/desy_syncnshare_consumption-plot_7.svg?job=plot_general)

##### desy_syncnshare_users_aai-plot_2.svg
![desy_syncnshare_users_aai-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/desy_syncnshare_users_aai-plot_2.svg?job=plot_general)

##### desy_syncnshare_users_all-plot_2.svg
![desy_syncnshare_users_all-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/desy_syncnshare_users_all-plot_2.svg?job=plot_general)


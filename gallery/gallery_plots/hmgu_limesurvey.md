# LimeSurvey (HMGU)

##### hmgu_limesurvey-plot_2.svg
![hmgu_limesurvey-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hmgu_limesurvey-plot_2.svg?job=plot_general)

##### hmgu_limesurvey-plot_3.svg
![hmgu_limesurvey-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hmgu_limesurvey-plot_3.svg?job=plot_general)

##### hmgu_limesurvey-plot_4.svg
![hmgu_limesurvey-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hmgu_limesurvey-plot_4.svg?job=plot_general)

##### hmgu_limesurvey-plot_5.svg
![hmgu_limesurvey-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hmgu_limesurvey-plot_5.svg?job=plot_general)

##### hmgu_limesurvey-plot_6.svg
![hmgu_limesurvey-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hmgu_limesurvey-plot_6.svg?job=plot_general)

##### hmgu_limesurvey-plot_7.svg
![hmgu_limesurvey-plot_7.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hmgu_limesurvey-plot_7.svg?job=plot_general)

##### hmgu_limesurvey-plot_8.svg
![hmgu_limesurvey-plot_8.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hmgu_limesurvey-plot_8.svg?job=plot_general)

##### hmgu_limesurvey-plot_9.svg
![hmgu_limesurvey-plot_9.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hmgu_limesurvey-plot_9.svg?job=plot_general)


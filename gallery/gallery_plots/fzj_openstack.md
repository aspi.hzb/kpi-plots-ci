# OpenStack (FZJ)

##### fzj_openstack-plot_2.svg
![fzj_openstack-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/fzj_openstack-plot_2.svg?job=plot_general)

##### fzj_openstack-plot_3.svg
![fzj_openstack-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/fzj_openstack-plot_3.svg?job=plot_general)

##### fzj_openstack-plot_4.svg
![fzj_openstack-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/fzj_openstack-plot_4.svg?job=plot_general)

##### fzj_openstack-plot_5.svg
![fzj_openstack-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/fzj_openstack-plot_5.svg?job=plot_general)

##### fzj_openstack-plot_6.svg
![fzj_openstack-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/fzj_openstack-plot_6.svg?job=plot_general)


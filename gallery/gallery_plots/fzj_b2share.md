# B2SHARE (FZJ)

##### fzj_b2share-plot_2.svg
![fzj_b2share-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/fzj_b2share-plot_2.svg?job=plot_general)

##### fzj_b2share-plot_3.svg
![fzj_b2share-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/fzj_b2share-plot_3.svg?job=plot_general)

##### fzj_b2share-plot_4.svg
![fzj_b2share-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/fzj_b2share-plot_4.svg?job=plot_general)


# Mattermost (HZDR)

##### hzdr_mattermost-plot_2.svg
![hzdr_mattermost-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_mattermost-plot_2.svg?job=plot_general)

##### hzdr_mattermost-plot_3.svg
![hzdr_mattermost-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_mattermost-plot_3.svg?job=plot_general)

##### hzdr_mattermost-plot_4.svg
![hzdr_mattermost-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_mattermost-plot_4.svg?job=plot_general)

##### hzdr_mattermost-plot_5.svg
![hzdr_mattermost-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_mattermost-plot_5.svg?job=plot_general)


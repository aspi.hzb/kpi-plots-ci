# REMS (DKFZ)

##### dkfz_rems-plot_2.svg
![dkfz_rems-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/dkfz_rems-plot_2.svg?job=plot_general)

##### dkfz_rems-plot_3.svg
![dkfz_rems-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/dkfz_rems-plot_3.svg?job=plot_general)

##### dkfz_rems-plot_4.svg
![dkfz_rems-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/dkfz_rems-plot_4.svg?job=plot_general)

##### dkfz_rems-plot_5.svg
![dkfz_rems-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/dkfz_rems-plot_5.svg?job=plot_general)

##### dkfz_rems-plot_6.svg
![dkfz_rems-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/dkfz_rems-plot_6.svg?job=plot_general)

##### dkfz_rems-plot_7.svg
![dkfz_rems-plot_7.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/dkfz_rems-plot_7.svg?job=plot_general)

##### dkfz_rems-plot_8.svg
![dkfz_rems-plot_8.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/dkfz_rems-plot_8.svg?job=plot_general)


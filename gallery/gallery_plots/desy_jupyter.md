# Jupyter (DESY)

##### desy_jupyter-plot_2.svg
![desy_jupyter-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/desy_jupyter-plot_2.svg?job=plot_general)

##### desy_jupyter-plot_3.svg
![desy_jupyter-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/desy_jupyter-plot_3.svg?job=plot_general)

##### desy_jupyter-plot_4.svg
![desy_jupyter-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/desy_jupyter-plot_4.svg?job=plot_general)


# Collabtex (HZDR)

##### hzdr_collabtex_active_users-plot_2.svg
![hzdr_collabtex_active_users-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_collabtex_active_users-plot_2.svg?job=plot_general)

##### hzdr_collabtex_active_users-plot_3.svg
![hzdr_collabtex_active_users-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_collabtex_active_users-plot_3.svg?job=plot_general)

##### hzdr_collabtex_active_users-plot_4.svg
![hzdr_collabtex_active_users-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_collabtex_active_users-plot_4.svg?job=plot_general)

##### hzdr_collabtex_active_users-plot_5.svg
![hzdr_collabtex_active_users-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_collabtex_active_users-plot_5.svg?job=plot_general)

##### hzdr_collabtex_active_users-plot_6.svg
![hzdr_collabtex_active_users-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_collabtex_active_users-plot_6.svg?job=plot_general)

##### hzdr_collabtex_active_users-plot_7.svg
![hzdr_collabtex_active_users-plot_7.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_collabtex_active_users-plot_7.svg?job=plot_general)

##### hzdr_collabtex_project_statistics-plot_2.svg
![hzdr_collabtex_project_statistics-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_collabtex_project_statistics-plot_2.svg?job=plot_general)

##### hzdr_collabtex_project_statistics-plot_3.svg
![hzdr_collabtex_project_statistics-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_collabtex_project_statistics-plot_3.svg?job=plot_general)

##### hzdr_collabtex_project_statistics-plot_4.svg
![hzdr_collabtex_project_statistics-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_collabtex_project_statistics-plot_4.svg?job=plot_general)

##### hzdr_collabtex_project_statistics-plot_5.svg
![hzdr_collabtex_project_statistics-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_collabtex_project_statistics-plot_5.svg?job=plot_general)

##### hzdr_collabtex_project_statistics-plot_6.svg
![hzdr_collabtex_project_statistics-plot_6.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_collabtex_project_statistics-plot_6.svg?job=plot_general)

##### hzdr_collabtex_project_statistics-plot_7.svg
![hzdr_collabtex_project_statistics-plot_7.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/hzdr_collabtex_project_statistics-plot_7.svg?job=plot_general)


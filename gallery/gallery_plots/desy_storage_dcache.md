# InfiniteSpace (DESY)

##### desy_storage_dcache-plot_2.svg
![desy_storage_dcache-plot_2.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/desy_storage_dcache-plot_2.svg?job=plot_general)

##### desy_storage_dcache-plot_3.svg
![desy_storage_dcache-plot_3.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/desy_storage_dcache-plot_3.svg?job=plot_general)

##### desy_storage_dcache-plot_4.svg
![desy_storage_dcache-plot_4.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/desy_storage_dcache-plot_4.svg?job=plot_general)

##### desy_storage_dcache-plot_5.svg
![desy_storage_dcache-plot_5.svg](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/desy_storage_dcache-plot_5.svg?job=plot_general)


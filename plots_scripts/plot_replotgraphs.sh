#!/bin/bash

# This script reproduces the graphs presented in various outlets (reporting etc), 
# with newest available data.

for dn in ../202[1-9]*/; do
    echo "Plotting in "$dn

    # execute hand-made scripts to reproduce graphs with most recent data
    pushd "$dn"
    ./plot.sh
    popd

    # move plots to artifacts folder in subfolders
    folder=$(echo $dn | cut -d "/" -f 2)
    mkdir -v ../plots/"$folder"
    mv -v "$dn"/*.svg ../plots/"$folder"
    mv -v "$dn"/*.png ../plots/"$folder"
    
    
done
#!/bin/bash

if [ ! -d ../plots/pdf ]; then  mkdir -pv ../plots/pdf; fi
if [ ! -d ../plots/svg ]; then  mkdir -pv ../plots/svg; fi


# create plot(s) for every data.csv found within data folder
for datafile in ../data/*/data.csv; do
{
  echo -e "\n------------------- ${datafile}"
  test -f ${datafile} || {
      echo "File  ${datafile}  does not exist, skipping"
      continue
  }
  # cut on last "/" to get folder name
  folder=$(echo $datafile | rev | cut -d "/" -f 2- | rev)
  folder=`dirname ${datafile}`
  basefile=`basename ${datafile}`

  # Clean up any remaining *_plot.csv if there are any. They must be created on the fly.
  if [ -e "${datafile%.csv}_plot.csv" ]; then
    rm -v "${datafile%.csv}_plot.csv"
  fi

  # If there is a filter.sh, use it.
  # Otherwise, link the standard filter.sh and use this.
  if [ ! -e "$folder/"filter.sh ] || [ -h "$folder/"filter.sh ]; then
    # remove the non-existing file, just to eliminate broken symlinks
    rm -f "$folder/"filter.sh
    echo ln -sv "$(pwd)"/filter.sh "$folder"/;
    ln -sv "$(pwd)"/filter.sh "$folder"/;
  fi
  pushd "$folder"
      ./filter.sh
  popd
  echo "Filter done"

  # Plotting.
  # Either
  # - we can use the standard plot.plt and link it there, or
  # - there is a specific plot.plt already deposited for the specific folder.

  if [ ! -e "$folder"/plot.plt ] || [ -h "$folder"/plot.plt ]; then
    # remove the non-existing file, just to eliminate broken symlinks
    rm -f "$folder"/plot.plt
    echo ln -sv "$(pwd)"/plot.plt "$folder"/;
    ln -sv "$(pwd)"/plot.plt "$folder"/;
  fi

  pushd "$folder"
    gnuplot plot.plt
  popd

  # Convert files as necessary, move plot files to artifacts folder, concatenate name.
  for plotfile in "$folder"/plot*.pdf; do
  {
    pdffile=$(echo $plotfile | cut -d "/" -f 3- | sed 's/\//-/g')
    mv -v "$plotfile" "$pdffile"
    mv -v "${plotfile%.pdf}.svg" "${pdffile%.pdf}.svg"

    # Move all files types to respective folders
    mv -v "$pdffile" ../plots/pdf/
    mv -v "${pdffile%.pdf}.svg" ../plots/svg/
  }
  done
}
done

#!/bin/gnuplot

# Check if the separator is a "," or ";". We want to have at least two columns, so we'll test:
set datafile separator ","
stats 'data_plot.csv' nooutput
# If we detected too few columns, try another separator.
if (STATS_columns<=1) {
  set datafile separator ";"
  stats 'data_plot.csv' nooutput
}

print "Detected ".STATS_columns." columns."

if (STATS_columns<=1) {
  print "Detected too few columns. Exiting."
  exit
}

set yrange [0:*]
set xdata time
set format x "%Y-%m-%d"


data_fmt="%Y-%m-%d %H:%M"
set timefmt data_fmt

#set xrange ["2019-07-01":"2021-09-02"]
set xtics rotate by 45
set key top left
set linetype 2 dashtype 2


# set some coordinates for diagram formatting. Can also be used to draw multiple aligned diagrams in one canvas, if wanted (multiplot)
MP_LEFT = .1
MP_RIGHT = .95
MP_BOTTOM = .14
MP_TOP = .95
MP_xGAP = 0.1
MP_yGAP = 0.02


# Plot all columns: One file for each column.

do for [i=2:STATS_columns] {

    # since replot does not work reliably, we repeat the whole plot sequence
    # explicitly for every output format wanted.

    do for [IDX = 0:1] {
        if (IDX==0) {
            set terminal pdf  color noenhanced size 7,5 dashed
            set output 'plot_'.i.'.pdf'
            set xtics textcolor rgbcolor "black" offset -4,-3.5
        } else {
            set terminal svg
            set output 'plot_'.i.'.svg'
            set xtics textcolor rgbcolor "black" offset -4,-3
        }

        set multiplot layout 1,1 columnsfirst margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP
        set grid
        set yrange [0:*]

        plot 'data_plot.csv' u 1:i w lp pt 7 ps 0.25 lc black title columnheader

        unset multiplot
    }
}

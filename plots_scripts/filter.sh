#!/bin/bash

if [ -e prefilter.sh ]; then
    rm -v data_2.csv
    ./prefilter.sh v > data_2.csv
else
    ln -sv data.csv data_2.csv
fi

# still need to take care of different header types
cat data_2.csv | sed -e 's/;/,/g' | sed -e 's/, /,/g' | sed -e 's/NaN//g' | gawk 'BEGIN{FS=","
    # some defaults. first column has fixed title:
    header[1]="date-time";

    # all columns to be printed by default, until deactivated explicitly below
    for (i=1; i<=1000; i++)
        printit[i]=1;
}{
    if (substr($1,0,1)=="#") {
        for (i=2; i<=NF; i++)
        {
            # extract from 3 header lines:
            # 1) column title
            # 2) whether or not to plot this column (1/0)
            # 3) unit

            switch (headcounter) {
                case 0: header[i]=$i; break;
                case 1: printit[i]=$i; break;
                case 2:
                    # Add unit to the header text, if there is any unit, otherwise leave as-is.
                    if ($i!="")
                        header[i]=header[i] " [" $i "]";
                    break;
            }
            if (i>imax) imax=i;
        }
        headcounter++;

        # once we gathered all 3 headlines, we can print the newly constructed standard header
        if (headcounter==3) {
            for (i=1; i<=NF; i++) {
                if (printit[i]) {
                if (i>1) printf(",");
                printf("\"%s\"",header[i]);
                }
            }
            printf ("\n");
        }
    } else if (NF>1) {
        if (FNR>1)
            datetime=substr($1,1,4)"-"substr($1,6,2)"-"substr($1,9,2);
        else
            datetime=$1
        printf ("%s",datetime);
        for (i=2; i<=NF; i++) {

            # yet another special case. If, in the first line, the keyword is "comment", dont print that column
            if (NR==1 && $i=="comment")
            {
                printit[i]=0;
                continue
            }

            if (printit[i]) {
                printf (",%s",$i);
            }
        }
        printf ("\n");
    } else {
        print
    }

}' > data_plot.csv
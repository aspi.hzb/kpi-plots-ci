# Service Usage Plots

In this project, plots are generated from service usage data.
The data is incorporated via submodules from separate projects.


##  Generated Plots (most recent)

All plots are generated automatically when changes are detected or the generation pipeline is triggered.
Also daily plots are generated.

### General overview plots

* [**Latest build, all graphs**](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/browse/plots?job=plot_general)
* [Download all graphs in zip file](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/download/plots?job=plot_general)
* [Previously used graphs](example_graphs.md), used in reports, reproduced with most recent data.
* [Gallery Overview](gallery/gallery.md)
* [Graphs pdf collection](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/all.pdf?job=plot_general)
* Example of inline svg (AAI users):  
  ![AAI users](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots/svg/aai_users_per_centre-plot_cumulated_with_overall.svg?job=plot_general)

### KPI plots
* [**Latest build, all graphs**](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/browse/plots_kpi?job=plot_general)
* [Download all graphs in zip file](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/download/plots_kpi?job=plot_general)
* [KPI Gallery Overview](gallery/gallery.md#overview-of-kpi-plots)
* [Graphs pdf collection](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/all.pdf?job=plot_general)
* [Overall Cloud KPI result](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/file/plots_kpi/overall-kpi.txt?job=plot_general)
    * [Contribution per service](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/file/results/kpi_per_service.pdf?job=plot_general)
    * [Contribution of single service / KPI components](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/file/plots_kpi/_OVERALL_USAGE_overview.pdf?job=plot_general)
    * see [here for documentation of the Helmholtz Cloud Service Operation KPI](https://hifis.net/doc/cloud-service-kpi/).
* Contributions of services to overall KPI:  
  ![Bar plot of contributions of services to overall KPI](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/results/kpi_per_service.svg?job=plot_general)
* Example of KPI plot (HZDR Codebase):  
  ![HZDR codebase users](https://codebase.helmholtz.cloud/hifis/overall/kpi/kpi-plots-ci/-/jobs/artifacts/master/raw/plots_kpi/svg/hzdr_codebase_active_users-plot_4.svg?job=plot_general)

## Local execution

Clone this repository *recursively, i.e. including the data*, with:
```
git clone --recursive git@codebase.helmholtz.cloud:hifis/overall/kpi/kpi-plots-ci.git
```

If you have cloned the repository already, you might need to update the submodules (i.e., data) with
```
git submodule sync --recursive
git submodule update --remote --recursive --init
```

Necessary for converting and plotting: `gnuplot`, `gawk`, `imagemagick`, `poppler-utils`.

```code
cd plots_scripts      # or any other of the sub-folders
./plot.sh
```

## Structure
* `subprojects`: Linked projects providing the source data. Internal structure is *very* heterogenous atm.
* `data`: Collected raw data (links) from subprojects.
    - `servicename`: One Level of subfolders with one service per folder.
        - `data.csv` (mandatory): Raw data with one date/time column, and at least one data column
        - `prefilter.sh` (optional): Provide specific filtering/rewrite for non-standard date/time formats, if needed. Standard is `%Y-%m-%d %H:%M`.
        - `filter.sh` (optional): Provide filter/data integration script, if needed. Otherwise raw data is plotted.
* `kpi-weights.csv`: Weighting of all separate service contributions to overall KPI; usually 50% service users+50% service usage.
* `plots`: Collection of all plots to be output => Artifacts folder

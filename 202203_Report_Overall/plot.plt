#!/bin/gnuplot

set datafile separator ","
set output './Report2021_plot1.svg'

set yrange [0:*]

set xdata time
set timefmt "%Y-%m-%d %H:%M"
set format x "%Y-%m-%d"
set xrange ["2019-07-01":*]
set grid
set xtics rotate by 45 offset -4,-3
set key top left


set terminal svg size 1100,300
if (!exists("MP_LEFT"))   MP_LEFT = 0.08
if (!exists("MP_RIGHT"))  MP_RIGHT = .99
if (!exists("MP_BOTTOM")) MP_BOTTOM = .25
if (!exists("MP_TOP"))    MP_TOP = .99
if (!exists("MP_xGAP"))   MP_xGAP = 0.09
if (!exists("MP_yGAP"))   MP_yGAP = 0.02


set multiplot layout 1,3 columnsfirst margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP


set timefmt "%d.%m.%Y"
set ylabel "Helmholtz AAI users"
plot '../stats/aai/aai_users.csv'       u 1:2 w l lc black lw 2 title 'total'

set datafile separator ";"
set ylabel "Helmholtz Codebase (Gitlab) users\n(incl. local users)"
set timefmt "%Y-%m-%d %H:%M"
plot '../stats/gitlab/active_users.csv' u 1:2 w l lc black lw 2 title 'total' ,\
     ''                                 u 1:7 w l lc rgb "#005aa0" lw 0.8 title 'last 60d'

set datafile separator ","
set ylabel "Course participant-hours"
set timefmt "%Y-%m"
set style fill pattern 2
plot '../stats/workshops/data.csv' u 1:4 w l lc black lw 2 title 'cumulative (per year)' ,\
     ''                                                     u 1:2 w boxes lc rgb "#005aa0" lw 0.8 title 'per month'

unset multiplot

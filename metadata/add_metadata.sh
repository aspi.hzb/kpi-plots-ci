#!/bin/bash

# for every text file in here, depicting one service onboarding time each....
for i in */*; do
    # ...we look for corresponding data files, with arbitary sub-KPI naming
    basename="$(echo $i | cut -d "/" -f 1)"
    for j in ../data/${basename}*; do
        if [ ! -e "$j"/service_metadata ]; then
            ln -sv ../../metadata/"$basename"/ "$j"/service_metadata
        fi
    done
done
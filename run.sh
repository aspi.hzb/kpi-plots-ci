#!/bin/bash

rm -vRf ./results
mkdir -pv ./results

# headers for the main result files

echo "#Service name,Sum of weights (should be 1),KPI,T_Report" > results/kpi_per_service.csv
echo "#T_report,Sum of weights that should equal to number of weighted services,Total KPI,KPI per service" > results/results.csv

# Check if we have a file depicting T_report date(s).
# If not, put today in as the only T_report  date

if [ ! -e metadata/T_report_collection.txt ]; then
    echo "Setting T_report to today:"
    date +"%Y-%m-%d" | tee metadata/T_report_collection.txt
fi

for Treports in $(cat metadata/T_report_collection.txt); do

    # delete all intermediate files on every lap
    if [ -e ./plots ]; then
        rm -vRf ./plots/* 
    fi
    if [ -e ./plots_kpi ]; then
        rm -vRf ./plots_kpi/*
    fi
    
    echo $Treports

    echo -e -n "T_report=\""$Treports"\"" > data/T_report.txt

    pushd plots_kpi_scripts
    bash ./plot.sh
    popd
done

# Final plots only for the last date
pushd plots_scripts
bash ./plot.sh
bash ./plot_replotgraphs.sh
popd

pdfunite plots/pdf/*.pdf plots/all.pdf
pdfunite plots_kpi/pdf/*.pdf plots_kpi/all.pdf

#!/bin/gnuplot

# Data file to plot
filename='data_plot.csv'

# Check if the separator is a "," or ";". We want to have at least two columns, so we'll test:
sep=","
set datafile separator sep
stats filename nooutput
# If we detected too few columns, try another separator.
if (STATS_columns<=1) {
  sep=";"
  set datafile separator sep
  stats filename nooutput
}

print "Detected ".STATS_columns." columns."

if (STATS_columns<=1) {
  print "Detected too few columns. Exiting."
  exit
}

set yrange [0:*]
set xdata time
set format x "%Y-%m-%d"

data_fmt="%Y-%m-%d %H:%M"
set timefmt data_fmt

set xtics rotate by 45

set key top left
set linetype 2 dashtype 2


# set some coordinates for diagram formatting. Can also be used to draw multiple aligned diagrams in one canvas, if wanted (multiplot)
MP_LEFT = .1
MP_RIGHT = .95
MP_BOTTOM = .14
MP_TOP = .93
MP_xGAP = 0.1
MP_yGAP = 0.02



# fitting params
set fit errorscaling errorvariables
FIT_LIMIT=1e-15     # this should help to fit even very degenerate plots
FIT_MAXITER=1000

# fitting function
susage(x) = m*x + y0

# Plot all columns: One file for each column.

# since replot does not work reliably, we repeat the whole plot sequence
# explicitly for every output format wanted.

# log to one file per service
set print "susage.csv"

do for [i=2:STATS_columns] {

    # starting parameters for each fit
    y0=100
    m=1

    # fit slope for data points at least from 2020 (to exclude any possible data points from 2019 or before)
    # for annual reporting 2021: AND fit only for data that is <=2021

    fmt = "%Y-%m-%d"
    set xdata time

    # reporting date (end of measurement period)
    T_report="2022-07-01"   # default: hard-coded for every report
    # if there is a specific file determining another T_report, use that instead:
    if (system("[ ! -f \"..\/T_report.txt\" ]; echo $?")) {
        load "../T_report.txt"
    }

    # starting dates.
    T_logstart=system("cat ".filename." | tr -d '\r' | awk 'BEGIN{FS=\"".sep."\"}{if (NR>1 && NF>1) {print $1; exit;}}'")


    # Onboarding date can be no earlier than the start of the pilot Helmholtz Cloud, i.e. 2021-03-29.
    # https://hifis.net/news/2021/03/29/helmholtz-cloud-beta

    T_onboard="2021-03-29"
    # if there is a specific file determining another T_onboard for that service, defined manually, use that instead:
    if (system("[ ! -f service_metadata/T_onboard ]; echo $?")) {
        load "service_metadata/T_onboard"
    }

    if (strptime(fmt, T_onboard) > strptime(data_fmt, T_logstart)) {
        T_fitstart_overall=T_onboard
        T_fitstart_overall_fmt=strptime(fmt, T_onboard)
    } else {
        T_fitstart_overall=T_logstart
        T_fitstart_overall_fmt=strptime(data_fmt, T_logstart)
    }

    # Do the fitting
    fit [T_fitstart_overall_fmt:strptime(fmt, T_report)] susage(x) filename u 1:i via m,y0


    # Later: Yearly procedure for yearly reports, not defined for 2021 annual report yet.
    # Will take care of this later for reporting 2023 ff.
    # But we need a placeholder here.
    T_fitstart_yearly="N/A"
    T_reference_yearly="N/A"

    # Now we have a fit and can decide where to put the T_reference_overall.
    # Per default, T_reference_overall should be equal to T_fitstart_overall.
    # Exceptions MAY be that the KPI at this date is very low (close to zero), such that a division by this KPI makes no sense.
    # In that case, a separate date MAY be defined by Cloud Cluster Management in T_reference_overall.txt.
    T_reference_overall=T_fitstart_overall
    T_reference_overall_fmt=T_fitstart_overall_fmt
    if (system("[ ! -f T_reference_overall.txt ]; echo $?")) {
        load "T_reference_overall.txt"
        T_reference_overall_fmt=strptime(fmt, T_reference_overall)
    }

    # Using T_reference_overall and T_report, the corresponding KPIs at these dates are calculated from the fit.
    KPI_e=susage(strptime(fmt, T_report))
    KPI_a=susage(T_reference_overall_fmt)

    # get minimum and maximum of this column within considered time range with awk hack.
    # gnuplot stats itself does not seem to support to extract conditional minima.

    minimum=1.0*system("awk -v ts=".T_fitstart_overall." -v te=".T_report." -v i=".i." 'BEGIN{min=1e308;FS=\",\"}{if ($1>=ts && $1<=te) if ($i<min) min=$i}END{printf(\"%g\",min)}' ".filename);

    print "minimum: "
    print minimum

    maximum=1.0*system("awk -v ts=".T_fitstart_overall." -v te=".T_report." -v i=".i." 'BEGIN{max=-1e308;FS=\",\"}{if ($1>=ts && $1<=te) if ($i>max) max=$i}END{printf(\"%g\",max)}' ".filename);

    print "maximum: "
    print maximum



    # if the extrapolated start value of KPI is smaller/larger than the actually reported minimum/maximum, use the latter instead.
    if (KPI_a < minimum) {
       KPI_a=minimum
    }
    if (KPI_e > maximum) {
       KPI_e=maximum
    }
    # Compute the raw dollar value
    # only if KPI_a if larger than 0
    if (KPI_a > 1e-6) {
       kpi_raw=KPI_e/KPI_a-1
    } else {
       kpi_raw=0
    }

    # check if we actually want to consider this value (service was on board long enough, i.e. >=90 days)
    if ((strptime(fmt, T_report)-T_fitstart_overall_fmt) >= (90.*3600.*24.)) {
        kpi_raw_title=sprintf("%.3g - subject to weighting.", kpi_raw);
    } else {
        kpi_raw_title=sprintf("%.3g - NOT CONSIDERED: less than 90 days data available.", kpi_raw);
        kpi_raw=-1000;
    }


    # Extract KPI name. Seemingly, the header name of the column cannot be directly derived with gnuplot,
    # instead we have to circle via shell and pipes (urgh)
    kpi_name=system("head -n 1 ".filename." | tr -d '\r' | awk 'BEGIN{FS=\"".sep."\"}{print $".i."}'")

    # Printing to table
    logdata=sprintf("\"%s\",%s,%d,%s,%g,%g,%g,%g,,%s,%s,%s,%s,%s,%s,%s,%g,%g,%g\n",displayname,foldername,i,kpi_name,m,m_err,y0,y0_err,T_logstart,T_onboard,T_fitstart_overall,T_reference_overall,T_fitstart_yearly,T_reference_yearly,T_report,KPI_a,KPI_e,kpi_raw)
    print logdata

    # Plotting (once to pdf, once to svg)
    do for [IDX = 0:1] {
        if (IDX==0) {
            set terminal pdf  color noenhanced size 7,5 dashed
            set output 'plot_'.i.'.pdf'
            set xtics textcolor rgbcolor "black" offset -4,-3.5
        } else {
            set terminal svg noenhanced size 800,600
            set output 'plot_'.i.'.svg'
            set xtics textcolor rgbcolor "black" offset -4,-3
        }

        set multiplot layout 1,1 columnsfirst margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP
        set title displayname

        # print linear fitting variables and errors
        set label 1 sprintf("m=%.2g +- %.3g (%.3g %%),\ny0=%.2g +- %.3g (%.3g %%)\nT_reference_overall=%s, T_report=%s\nkpi-raw=%s",m,m_err,m_err/m*100,y0,y0_err,y0_err/y0*100,T_reference_overall,T_report,kpi_raw_title) left at graph 0.05, graph 0.85

        set timefmt fmt
        set xrange ["2020-01-01":*]
        set xtics '2020-01-01 00:00',365*24*3600
        set mxtics 12
        set grid xtics nomxtics ytics
        set timefmt data_fmt

        set label 2 "" at strptime(fmt, T_onboard),susage(strptime(fmt, T_onboard)) point pt 8  ps 1 lw 1 lc "#000000"
        set label 3 "" at strptime(data_fmt, T_logstart),susage(strptime(data_fmt, T_logstart)) point pt 2  ps 1 lw 1 lc "#888888"
        set label 4 "" at T_fitstart_overall_fmt,susage(T_fitstart_overall_fmt) point pt 6  ps 1 lw 1 lc "#00bb00"
        set label 5 "" at T_reference_overall_fmt,KPI_a point pt 4  ps 1 lw 1 lc "#0000bb"
        set label 6 "" at strptime(fmt, T_report),KPI_e point pt 10 ps 1 lw 1 lc "#ff0000"

        set arrow from T_reference_overall_fmt,KPI_a to strptime(fmt, T_report),KPI_e nohead linewidth 4 lt 2 lc "#55bb55"

        plot filename u 1:i w lp pt 7 ps 0.25 lc black title columnheader ,\
             (x>=T_fitstart_overall_fmt && x<= strptime(fmt, T_report)) ? susage(x) : 1/0 w l linewidth 1 lt 2 lc "#aaaaaa" notitle

        unset multiplot
        unset label
        unset arrow
    }
}

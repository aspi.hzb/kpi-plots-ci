#!/bin/bash

# form a matrix from the concatenated results file
tail -n +2 ../results/kpi_per_service.csv | gawk -v FS="," '{
    # kpi[service-displayname][date]=kpi
    kpi[$1][$4]=$3
    datetimes[$4]=$4
}END{
    # sort columns
    datetimes_n = asort(datetimes)

    # header of the matrix-table
    printf ("service") > "../results/kpi_per_service_matrix.csv"
    for (i = 1; i <= datetimes_n; i++)
        printf (",%s",datetimes[i]) > "../results/kpi_per_service_matrix.csv"
    printf ("\n") > "../results/kpi_per_service_matrix.csv"

    for (sn in kpi) {
        printf ("%s",sn)
        for (i = 1; i <= datetimes_n; i++)
            printf (",%s",kpi[sn][datetimes[i]])

        printf ("\n");
    }
}' | sort -f | tee -a ../results/kpi_per_service_matrix.csv

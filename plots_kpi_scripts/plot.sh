#!/bin/bash
if [ ! -d ../plots_kpi/pdf ]; then  mkdir -pv ../plots_kpi/pdf; fi
if [ ! -d ../plots_kpi/svg ]; then  mkdir -pv ../plots_kpi/svg; fi
if [ ! -d ../plots_kpi/log ]; then  mkdir -pv ../plots_kpi/log; fi




# define end-of-reporting time.
# if there is already an explicit file, leave as-is:
if [ ! -e ../data/T_report.txt ]; then
   # if not, write current date as end-of-report time, to compute the KPI of today
   echo -n "T_report=\""$(date +"%Y-%m-%d")"\"" > ../data/T_report.txt
fi

# create plot(s) for every data.csv found within data folder
for datafile in ../data/*/data.csv; do
{
  echo -e "\n------------------- ${datafile}"
  test -f ${datafile} || {
      echo "File  ${datafile}  does not exist, skipping"
      continue
  }
  # cut on last "/" to get folder name
  folder=$(echo $datafile | rev | cut -d "/" -f 2- | rev)
  folder=`dirname ${datafile}`
  basefile=`basename ${datafile}`

  # Clean up any remaining *_plot.csv if there are any. They must be created on the fly.
  if [ -e "${datafile%.csv}_plot.csv" ]; then
    rm -v "${datafile%.csv}_plot.csv"
  fi

  # If there is a filter.sh, use it.
  # Otherwise, link the standard filter.sh and use this.
  if [ ! -e "$folder/"filter.sh ] || [ -h "$folder/"filter.sh ]; then
    # remove the non-existing file, just to eliminate broken symlinks
    rm -f "$folder/"filter.sh
    echo ln -sv "$(pwd)"/filter.sh "$folder"/;
    ln -sv "$(pwd)"/filter.sh "$folder"/;
  fi
  pushd "$folder"
      ./filter.sh
  popd
  echo "Filter done"

  # Plotting.
  # Either
  # - we can use the standard plot.plt and link it there, or
  # - there is a specific plot.plt already deposited for the specific folder.

  if [ ! -e "$folder"/plot.plt ] || [ -h "$folder"/plot.plt ]; then
    # remove the non-existing file, just to eliminate broken symlinks
    rm -f "$folder"/plot.plt
    echo ln -sv "$(pwd)"/plot.plt "$folder"/;
    ln -sv "$(pwd)"/plot.plt "$folder"/;
  fi

  pushd "$folder"
    if [ -e fit.log ]; then rm -v fit.log; fi
    
    foldername=`basename ${folder}`
    # Our display-servicename is either the folder name...
    displayname=$foldername
    
    # or if it's in the metadata, the explicitly stored displayname
    if [ -d service_metadata ]; then
      if [ -e service_metadata/displayname ]; then
        displayname=`cat service_metadata/displayname`
      fi
    fi
    gnuplot -e "foldername='$foldername'" -e "displayname='$displayname'" plot.plt
  popd

  # Convert files as necessary, move plot files to artifacts folder, concatenate name.
  for plotfile in "$folder"/plot*.pdf; do
  {
    if [ -e "$plotfile" ]; then
    pdffile=$(echo $plotfile | cut -d "/" -f 3- | sed 's/\//-/g')
    mv -v "$plotfile" "$pdffile"
    mv -v "${plotfile%.pdf}.svg" "${pdffile%.pdf}.svg"

    # Move all files types to respective folders
    mv -v "$pdffile" ../plots_kpi/pdf/
    mv -v "${pdffile%.pdf}.svg" ../plots_kpi/svg/

    mv -v "$folder"/fit.log ../plots_kpi/log/"${pdffile%.pdf}.fit.log"
    mv -v "$folder"/susage.csv ../plots_kpi/log/"${pdffile%.pdf}.susage.csv"
    fi
  }
  done
}
done

# collect dollars, add kpi name in first column, skip empty lines
pushd ../plots_kpi/log
{
    echo "service_display_name,folder_name,col_number,kpi,m,m_err,y0,y0_err,,T_logstart,T_onboard,T_fitstart_overall,T_reference_overall,T_fitstart_yearly,T_reference_yearly,T_e,KPI_a,KPI_e,dollar-raw"
    for i in *.susage.csv; do
        if [ -e "$i" ]; then
            gawk -v fname=${i%.susage.csv} 'BEGIN{FS=","}{
                # exclude empty lines, lines with no data
                if (NF>1) printf("%s\n",$0)
            }' "$i"
        fi
    done
} > ../_OVERALL_USAGE-raw.csv

# weight KPI
cd ..
gawk 'BEGIN{FS=","; file_id=0;}{
    if (FNR<FNR_old) file_id++;
    FNR_old=FNR

    if (file_id==0) {
        # when reading the index file:
        # read all weights into corresponding indexed field.
        idx=$1"-"$2
        weights[idx]=$4;
    } else if (FNR==1) {
        # when reading data file:
        # first line: plot title line plus additional columns
        printf ("%s,weight,weighted KPI\n",$0);
    } else {
        # when reading data file below title line.
        # print everything and additional weighted computation.
        idx=$2"-"$3
        # only if raw KPI is an actual value (could be empty if considered invalid, e.g. because of too short integration time)
        if ($19>-1000)
        {
            weighted_kpi=$19*weights[idx]
            KPI_overall+=weighted_kpi;
        }
        else
            weighted_kpi=""
        printf ("%s,%g,%s\n", $0, weights[idx], weighted_kpi);

    }

}END{
    printf("%g\n", KPI_overall) > "overall-kpi.txt"
}' "../kpi-weights.csv" "_OVERALL_USAGE-raw.csv" | tee _OVERALL_USAGE.csv
popd

pushd ../plots_kpi/

# Column 19: Raw KPI per subdata per service
# Column 20: Weight
# Column 21: Weightes KPI per subdata per service
# Sum up all Column 21 per service and relate them
gawk -v fnresults='../results/results.csv' -v FS="," '{
  # integrate only if we have a valid KPI value

  if ($21*1.0==$21) {
    T_report=$16
    sum_w+=$20
    sum_k+=$21
    service_w[$1]+=$20
    service_k[$1]+=$21
  }
  
}END{
  printf ("%s,%g,%g,%g\n", T_report, sum_w, sum_k, sum_k/sum_w) >> fnresults
  
  # print weights and sum of weighted kpi per service - only for those service actually weighted.
  for (key in service_k) {
    if (service_w[key]>0)
      printf ("%s,%g,%g,%s\n",key,service_w[key],service_k[key],T_report)
  }
}' "_OVERALL_USAGE.csv" | sort -f | tee -a ../results/kpi_per_service.csv

popd

# plot summary of weighted KPI
./results_transpose.sh
gnuplot plot_overall_kpi.plt
#!/bin/gnuplot

# Check if the separator is a "," or ";". We want to have at least two columns, so we'll test:
sep=","
set datafile separator sep

fn="../plots_kpi/_OVERALL_USAGE.csv"

set ylabel "Weighted KPI"
set y2label "Raw (unweighted) KPI"
set y2tics

MP_LEFT = .03
MP_RIGHT = .97
MP_BOTTOM = .35
MP_TOP = .93
MP_xGAP = 0.1
MP_yGAP = 0.02

set xtics rotate by 90
set xtics textcolor rgbcolor "black" offset 0,-14

set style fill solid 1.00
set style histogram clustered gap 2

# get statistics (min/max) of each data set
print "Statistics for Unweighted KPI:"
stats fn u 19:xtic(1) prefix "unweighted";
print "Statistics for Weighted KPI:"
stats fn u 21:xtic(1) prefix "weighted";

# add 10% to either side for better visibility
weighted_min=weighted_min*1.1
weighted_max=weighted_max*1.1
unweighted_min=unweighted_min*1.1
unweighted_max=unweighted_max*1.1

# scale unweighted plots according to the min/max ratio of the weighted plot so that they fit onto each other.
if (unweighted_min/weighted_min > unweighted_max/weighted_max) {
 unweighted_max=weighted_max*unweighted_min/weighted_min
} else {
 unweighted_min=weighted_min*unweighted_max/weighted_max
}

# set the axes
set yrange [weighted_min:weighted_max]
set y2range [unweighted_min:unweighted_max]


# Plotting (once to pdf, once to svg): all part kpi per service (all services)
do for [IDX = 0:1] {
    if (IDX==0) {
        set terminal pdf  color noenhanced size 20,7 dashed
        set output '../plots_kpi/_OVERALL_USAGE_overview.pdf'
    } else {
        set terminal svg noenhanced size 1500,750
        set output '../plots_kpi/_OVERALL_USAGE_overview.svg'
    }


    set multiplot layout 1,1 columnsfirst margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP
    plot fn u 21:xtic(1) axis x1y1 with histogram title 'weighted' ,\
         fn u 19:xtic(1) axis x1y2 with histogram title 'raw (unweighted)'
    unset multiplot
}

# Plotting (once to pdf, once to svg): cumulated kpi per service (only weighted services)
MP_LEFT = .07
MP_RIGHT = .97
MP_BOTTOM = .35
MP_TOP = .97
MP_xGAP = 0.1
MP_yGAP = 0.02

fn="../results/kpi_per_service_matrix.csv"

set style data histogram
set style histogram clustered gap 2
set style fill solid border -1
set boxwidth 0.8 relative

set grid ytics

set yrange [*:*]
unset y2tics
unset y2label
set ytics 1
set xtics textcolor rgbcolor "black" offset 0,-8
set ylabel 'KPI contribution per service'

do for [IDX = 0:1] {
    if (IDX==0) {
        set terminal pdf  color noenhanced size 10,7 dashed font "OpenSans, 20"
        set output '../results/kpi_per_service.pdf'
    } else {
        set terminal svg noenhanced size 1000,750
        set output '../results/kpi_per_service.svg'
    }

    set multiplot layout 1,1 columnsfirst margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP

    plot for [i=2:3] fn u i:xtic(1) title columnhead(i)

    unset multiplot
}
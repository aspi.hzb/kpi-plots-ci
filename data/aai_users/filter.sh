#!/bin/bash

# summing up columns 2 and 3, combined
in=data.csv
out=data_plot.csv

cat $in | awk 'BEGIN{
    FS=",";
    print("date-time,registered users");
}{
    if (NR>1) {
        # shuffle date and time manually such that it fits the standard,
        # in order to allow multiple plots together with other data sources
        datetime=substr($1,7,4) "-"  substr($1,4,2) "-" substr($1,0,2)

        # print actual cumulated data
        printf("%s,%g\n",datetime,a);
        a+=$2;
    }
}' > $out

# summing up all columns of all domains, resulting in overall "real" users (excluding functional accounts)
in=../../subprojects/AAI/stats/mail_domains.csv
out=data_plot_2.csv

cat $in | awk 'BEGIN{
    FS=",";
    print("date-time,registered users");
}{
    if (NR>1) {
        # print cumulated data per row
        a=0;

        for (col=2; col<=NF; col++) {
          a+=$col
        }

        printf("%s,%g\n",$1,a);
    }
}' > $out

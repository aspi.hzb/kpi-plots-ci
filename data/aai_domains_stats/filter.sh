#!/bin/bash

# count number of columns (i.e. number of centers) if respective user number exceeds thresholds (0,5,20)
cat data.csv | grep -v "#" | gawk 'BEGIN{
    FS=","

    # print header
    printf("date-time,total domains,at least 5 users,at least 20 users\n");
}{
    sum=sum5=sum20=0;
    for (i=2; i<=NF; i++) {
        if($i>0)
            sum++
        if($i>=5)
            sum5++
        if($i>=20)
            sum20++
    }
    printf("%s,%g,%g,%g\n",$1,sum,sum5,sum20)
}' > data_plot.csv

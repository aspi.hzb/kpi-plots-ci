#!/bin/bash

# Step one: Make table standard conform, exclude all "comment" / "do not plot" columns.
# same as standard filter.sh

# still need to take care of different header types
cat data.csv | sed -e 's/;/,/g' | sed -e 's/, /,/g' | gawk 'BEGIN{FS=","
    # some defaults. first column has fixed title:
    header[1]="date-time";

    # all columns to be printed by default, until deactivated explicitly below
    for (i=1; i<=1000; i++)
        printit[i]=1;
}{
    if (substr($1,0,1)=="#") {
        for (i=2; i<=NF; i++)
        {
            # extract from 3 header lines:
            # 1) column title
            # 2) whether or not to plot this column (1/0)
            # 3) unit

            switch (headcounter) {
                case 0: header[i]=$i; break;
                case 1: printit[i]=$i; break;
                case 2:
                    # in this specific case: do nothing. we do not want the extra units
                    break;
            }
            if (i>imax) imax=i;
        }
        headcounter++;

        # once we gathered all 3 headlines, we can print the newly constructed standard header
        if (headcounter==3) {
            for (i=1; i<=NF; i++) {
                if (printit[i]) {
                if (i>1) printf(",");
                printf("\"%s\"",header[i]);
                }
            }
            printf ("\n");
        }
    } else {
        printf ("%s",$1);
        for (i=2; i<=NF; i++) {

            # yet another special case. If, in the first line, the keyword is "comment", dont print that column
            if (NR==1 && $i=="comment")
            {
                printit[i]=0;
                continue
            }

            if (printit[i]) {
                printf (",%s",$i);
            }
        }
        printf ("\n");
    }

}' > data_temp1.csv


# Step 2:
#  2a) check for each column if there is at least one value exceeding a minimum threshold
#  2b) plot the table only for those columns that exceeded the threshold at least once; the others are summarized as "other"

cat data_temp1.csv | gawk 'BEGIN{FS=","
    # some defaults. first column has fixed title:
    header[1]="date-time";

    # all columns are marked to be invalid until proven otherwise below
    for (i=2; i<=1000; i++)
        printit[i]=0;

    # first column (date-time) to be printed, of course
    printit[1]=1;

    # reset column and line counter
    imax=1;
    j=0;

    # define our threshold
    threshold=20
}{
    # increase line counter
    j++;

    # go through all data, store and check if for every column, there is at least one value above or equal $threshold

    for (i=1; i<=NF; i++) {
        # store data for later re-visit
        data[i][j]=$i;

        # skip the rest if we are on either headline or first (date-time) column
        if (i==1 || j==1)
           continue;

        # check if, $threshold is exceeded, mark printit=valid if yes.
        if ($i>=threshold) printit[i]=1;

        # check for max. column counter
        if (i>imax) imax=i;
    }
}END{
    jmax=j;

    #for (i=1; i<=imax; i++) printf ("%d,",printit[i]); printf("\n");

    for (j=1; j<=jmax; j++) {
        if (j==1) {
           rowsum="Other"
        } else {
            rowsum=0;
            for (i=1; i<=imax; i++) {
                if (!printit[i]) {
                    rowsum+=data[i][j];
                }
            }
        }

        # print first two columns
        printf("%s,%s,",data[1][j],rowsum);

        # print all "valid" columns, further sum up to allow to stack plots
        for (i=2; i<=imax; i++) {
            if (printit[i]) {
                if (j==1) {
                   printf ("%s,",data[i][j])
                } else {
                   rowsum+=data[i][j];
                   printf ("%s,",rowsum)
                }
            }
        }
        printf ("\n");
    }


}' > data_cumulated.csv
#!/bin/gnuplot

# Check if the separator is a "," or ";". We want to have at least two columns, so we'll test:
set datafile separator ","
stats 'data_cumulated.csv' nooutput
# If we detected too few columns, try another separator.
if (STATS_columns<=1) {
  set datafile separator ";"
  stats 'data_cumulated.csv' nooutput
}

print "Detected ".STATS_columns." columns."

if (STATS_columns<=1) {
  print "Detected too few columns. Exiting."
  exit
}

set yrange [0:*]
set ylabel "Number of users" offset 2,0
set xdata time
set format x "%Y-%m-%d"


set timefmt "%Y-%m-%d %H:%M"

set xrange ["2022-09-01":*]
set xtics rotate by 45
set key top left
set linetype 2 dashtype 2


# set some coordinates for diagram formatting. Can also be used to draw multiple aligned diagrams in one canvas, if wanted (multiplot)
MP_LEFT = .1
MP_RIGHT = .95
MP_BOTTOM = .14
MP_TOP = .95
MP_xGAP = 0.1
MP_yGAP = 0.02


set label "No centre-\nspecific\ndata available" at (2021.6-1970)*365.25*24*3600,2000 center

# for better comparison, do same plot with overall users.
do for [IDX = 0:1] {
    if (IDX==0) {
        set terminal pdf  color noenhanced size 7,5 dashed
        set output 'plot_cumulated_with_overall.pdf'
        set xtics textcolor rgbcolor "black" offset -4,-3.5
    } else {
        set terminal svg size 900,500
        set output 'plot_cumulated_with_overall.svg'
        set xtics textcolor rgbcolor "black" offset -4,-3
    }

    set multiplot layout 1,1 columnsfirst margins screen MP_LEFT, MP_RIGHT, MP_BOTTOM, MP_TOP spacing screen MP_xGAP, MP_yGAP
    set key left inside font "OpenSans,10"
    set grid
    set yrange [0:*]

    plot '../aai_users/data_plot.csv' u 1:2 w lines lt 1 lw 2 lc rgb "#000000" notitle ,\
         for [i=STATS_columns:2:-1] 'data_cumulated.csv' u 1:i w filledcurves x1 title columnheader

    unset multiplot
}

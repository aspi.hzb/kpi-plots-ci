#!/bin/bash

cat data.csv | sed -e 's/;/,/g' | sed -e 's/, /,/g' | gawk 'BEGIN{FS=","
}{
    if (substr($1,0,1)=="#" || FNR==1) {
        print $0
    } else if (NF>1) {
        array datetime[3]
        split($1,datetime,"-")
        printf ("%04d-%02d-01",datetime[1],datetime[2]);
        for (i=2; i<=NF; i++) {
            printf (",%s",$i);
        }
        printf ("\n");
    } else {
        print
    }
}'
